//
//  LoginViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/10/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var usernameField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after  the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(sender: AnyObject) {
        
        let username = usernameField.text
        let password = passwordField.text
        
        let passwordString = "\(username!):\(password!)"
        let passwordData = passwordString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        let base64Credential = passwordData!.base64EncodedStringWithOptions([])
        
        let urlPath = "http://\(InfoManager.sharedInstance.url)/api/login/token"
        let url: NSURL = NSURL(string: urlPath)!
        let request = NSMutableURLRequest(URL: url)
        request.setValue("Basic \(base64Credential)", forHTTPHeaderField: "Authorization")
        request.HTTPMethod = "GET"
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let authString = "Basic \(base64Credential)"
        config.HTTPAdditionalHeaders = ["Authorization": authString]
        let session = NSURLSession(configuration: config)
        
        session.dataTaskWithRequest(request, completionHandler:{ (let data, let response, let error) in
            InfoManager.sharedInstance.loggedin = true
            if let _ = response as? NSHTTPURLResponse {
//                let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
//                print(dataString)

                var json1: AnyObject? = nil
                do{
                    json1 = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
//                    print(json1)
                }
                catch{
                    print("wtf")
                }
                
//                print(request)
                if let _: AnyObject = json1{
                    for each in (json1 as! NSDictionary)
                    {
                        if each.key as! String=="token"
                        {
                            InfoManager.sharedInstance.token = each.value as! String
//                            print(InfoManager.sharedInstance.token)
                            InfoManager.sharedInstance.shouldSetup = true
                            InfoManager.sharedInstance.username = username!

                            self.dismissViewControllerAnimated(true, completion: nil)

                        }
    //                    println(each.key)
                    }
                }
                if InfoManager.sharedInstance.token == ""
                {
                    print("login failed")
                }
            }
//            self.dismissViewControllerAnimated(true, completion: nil)
            
        }).resume()
        
        
        print("logging in")
//        InfoManager.sharedInstance.loggedin = true
//        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func register(sender: AnyObject) {
        let RegisterVC = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        self.presentViewController(RegisterVC, animated: true, completion: nil)
        
    }
}

