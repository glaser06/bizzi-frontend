//
//  Product.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/11/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import Foundation

class Product: NSObject {
    
    var name:String!

    var type: String!
    
    var details: String!
    
    var school:String!
    
    var seller:String!
    
    var price:String!
    
    var imgID:[String]!
    
    var image: UIImage!
    
    var listingID:String!
    
    var status: String!
    
//    var img
    
    init(name: String, type: String,price:String,seller:String,img:[String])
    {
        self.name = name
        self.type = type

        self.price = price
        self.seller = seller
        self.imgID = img

    }
    
}