//
//  TransactionViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/30/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class TransactionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        var doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Bordered, target: self, action: "done")
        self.navigationItem.rightBarButtonItem = doneButton
//        self.navigationItem.hidesBackButton = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func done()
    {
        var transNav = self.storyboard?.instantiateViewControllerWithIdentifier("TransactionNavController") as! UINavigationController
        
        self.navigationController?.viewControllers.first?.dismissViewControllerAnimated(true, completion: nil)
//        self.navigationController?.viewControllers.first?.presentingViewController.revealViewController().setFrontViewController(transNav, animated: true)
        var temp = self.navigationController?.viewControllers.first?.presentingViewController as! SWRevealViewController
        print(temp)
        temp.setFrontViewController(transNav, animated: true)
//        temp?.presentingViewController?.revealViewController().setFrontViewController(transNav, animated: true)
//        self.navigationController?.viewControllers[0].presentingViewController?.revealViewController().setFrontViewController(transNav, animated: true)
//        self.navigationController?.viewControllers.first?.reveal.setFrontViewController(transNav, animated: true)
//        self.navigationItem.hidesBackButton = false
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
