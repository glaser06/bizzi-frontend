//
//  CategoryViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/23/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {

    @IBOutlet weak var miscButton: UIButton!
    @IBOutlet weak var fashionButton: UIButton!
    @IBOutlet weak var techButton: UIButton!
    @IBOutlet weak var furnitureButton: UIButton!
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var applianceButton: UIButton!
    
    var types = ["book","furniture","tech","clothes","misc"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after  the view.
        bookButton.tag = 0
        furnitureButton.tag = 1
        techButton.tag = 2
        fashionButton.tag = 3
        miscButton.tag = 4
        applianceButton.tag = 5
        
        bookButton.layer.cornerRadius = 0.5 * bookButton.bounds.size.width
        furnitureButton.layer.cornerRadius = 0.5 * furnitureButton.bounds.size.width
        techButton.layer.cornerRadius = 0.5 * techButton.bounds.size.width
        fashionButton.layer.cornerRadius = 0.5 * fashionButton.bounds.size.width
        miscButton.layer.cornerRadius = 0.5 * miscButton.bounds.size.width
        applianceButton.layer.cornerRadius = 0.5 * applianceButton.bounds.size.width
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func selectCategory(sender: AnyObject) {

        if sender.tag == 0
        {
            let sellVC = self.storyboard?.instantiateViewControllerWithIdentifier("SellBookVC") as! BarcodeScanViewController
            self.navigationController?.pushViewController(sellVC, animated: true)
        }
        else
        {
            let sellVC = self.storyboard?.instantiateViewControllerWithIdentifier("SellViewController") as! SellViewController
            sellVC.category = types[sender.tag]
            self.navigationController?.pushViewController(sellVC, animated: true)
        }
        

    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
