//
//  InfoManager.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/10/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import Foundation

class InfoManager:NSObject {
    class var sharedInstance: InfoManager {
        struct Static {
            static var instance: InfoManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = InfoManager()
        }
        
        return Static.instance!
    }
    
    var loggedin:Bool = false
    
    var token:String{
        get{
            let token: AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey("token")
            if token == nil
            {
                return ""
            }
            return token as! String
        }
        set(newValue){
            NSUserDefaults.standardUserDefaults().setObject(newValue as NSString, forKey: "token")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    var username:String{
        get{
            let username: AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey("username")
            if username == nil
            {
                return ""
            }
            return username as! String
        }
        set(newValue){
            NSUserDefaults.standardUserDefaults().setObject(newValue as NSString, forKey: "username")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    var products = []
    
    var shouldSetup = true
    
    var currentProdcut:Product!
    
    var navBarColor:UIColor!
    
//    var url:String = "54.153.39.147"
    var url:String = "52.8.129.61"
    

}