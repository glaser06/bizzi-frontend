//
//  RegisterViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/10/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    @IBOutlet weak var usernameField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var schoolField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after  the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func register(sender: AnyObject) {
        
        let url = NSURL(string: "http://\(InfoManager.sharedInstance.url)/api/user/add")
        let urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        let params = ["username": usernameField.text!,"password": passwordField.text!,"school":schoolField.text!]

        let data: NSData!
        do{
            data = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        }
        catch{
            data = nil
        }
        let postData = NSData(data: data)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let uploadTask = NSURLSession.sharedSession().uploadTaskWithRequest(urlRequest, fromData: postData, completionHandler: {data,request,error -> Void in
            if error != nil
            {
                print(error)
                return
            }
            let json1: AnyObject?
            do {
                json1 = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves)
            }
            catch{
                json1 = nil
            }
            
//            print(request)

            
            self.dismissViewControllerAnimated(true, completion: nil)
            //            if json["logged in"]as Bool == true
            //            {
            //                println("aight")
            //            }
            
            
        })
        uploadTask.resume()
        self.view.endEditing(true)
//        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
}

