//
//  SellViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/11/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit
import MobileCoreServices

class SellViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate{

    @IBOutlet weak var imageView: UIButton!
    @IBOutlet weak var imageView2: UIButton!
    @IBOutlet weak var imageView3: UIButton!
    @IBOutlet weak var imageView4: UIButton!
    
    

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var priceField: UITextField!
//    @IBOutlet weak var typeField: UITextField!
//    @IBOutlet weak var schoolField: UITextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var sellButton: UIButton!
    @IBOutlet weak var conditionField: UITextView!


    @IBOutlet weak var conditionButton: UIButton!
    
    var typeDropDown: UIScrollView!
    
    var conditionDropDown: UIScrollView!
    
    var newMedia:Bool!
    
    var category:String!
    
    var isbn:String!
    
    var currImage:Int = 1
    var imageViewList:[UIButton] = []
    
    var conditionToggle = false
    
    var googleKey = "AIzaSyBgGMME40uhmkp5m0EMSsg5c79kOTQmRaQ"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 65))
//        navBar.barTintColor = MainListViewController.bizziColor
//        
//        self.view.addSubview(navBar)
//        
//        
//        //        self.navigationController?.navigationBar.height = 44
//        let cancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancel:")
//        let navItem = UINavigationItem(title: "Sell")
//        navItem.leftBarButtonItem = cancel
//        navBar.items = [navItem]
//        UIBarButtonItem.appearance().tintColor = UIColor.blueColor()
        nameField.delegate = self
        priceField.delegate = self
//        schoolField.delegate = self
        sellButton.backgroundColor = MainListViewController.bizziColor
        descriptionField.delegate = self
        descriptionField.layer.borderWidth = 1.0
        descriptionField.layer.borderColor = UIColor.lightGrayColor().CGColor
        conditionField.delegate = self
        conditionField.layer.borderWidth = 1.0
        conditionField.layer.borderColor = UIColor.lightGrayColor().CGColor
//        conditionField.delegate = self
   
        imageViewList = [imageView,imageView2,imageView3,imageView4]

        for (var i=0;i<imageViewList.count;i++)
        {
            imageViewList[i].tag = i
        }
        var tempFrame = conditionButton.frame
        
        self.conditionDropDown = UIScrollView(frame: CGRect(x: tempFrame.origin.x, y: tempFrame.origin.y+40, width: tempFrame.width, height: 0))
        makeConditionDropDown()
        self.view.addSubview(conditionDropDown)
//        typeButton.titleLabel?.textAlignment = NSTextAlignment.Left
        conditionButton.layer.borderColor = UIColor.lightGrayColor().CGColor
        conditionButton.layer.borderWidth = 1.0
        
//        self.typeDropDown = UIScrollView(frame: CGRect(x: 16, y: typeButton.frame.origin.y, width: self.view.frame.width*0.6, height: 0))
//        makeTypeDropDown()
//        self.view.addSubview(typeDropDown)
    
        // Do any additional setup after  the view.
    }
    override func viewWillAppear(animated: Bool) {
        if category == "book"
        {
            self.getBookData(isbn)
        }
    }
    func makeTypeDropDown()
    {
//        var filterTitle = UILabel(frame: CGRect(x: 0, y: 0, width: self.filterView.frame.width, height: 50))
//        filterTitle.text = "   Filter"
//        filterTitle.font = filterTitle.font.fontWithSize(20)
//        filterTitle.backgroundColor = UIColor.lightGrayColor()
        self.typeDropDown.backgroundColor = UIColor.whiteColor()
//        self.filterView.addSubview(filterTitle)
        var types = ["Book","Furniture","Tech","Fashion","Misc"]
        
        for(var i=0;i<5;i++)
        {
            let tempButton = UIButton(frame: CGRect(x: 0, y: CGFloat(40*i), width: typeDropDown.frame.width, height: CGFloat(40)))
            tempButton.setTitle("  \(types[i])", forState: UIControlState.Normal)
            tempButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
            tempButton.layer.borderWidth = 0.4
            tempButton.layer.borderColor = UIColor.lightGrayColor().CGColor
            tempButton.addTarget(self, action: "changeType:", forControlEvents: .TouchUpInside)
            tempButton.tag = i
            self.typeDropDown.addSubview(tempButton)
        }
    }
    func makeConditionDropDown()
    {
        self.conditionDropDown.backgroundColor = UIColor.whiteColor()
        var conditions = ["New","Like new", "Used", "Other"]
        for (var i=0;i<conditions.count;i++)
        {
            let tempButton = UIButton(frame:CGRect(x: 0, y: CGFloat(35*i), width:conditionDropDown.frame.width , height: CGFloat(35)))
            tempButton.setTitle("  \(conditions[i])", forState: .Normal)
            tempButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
            tempButton.layer.borderWidth = 0.4
            tempButton.layer.borderColor = UIColor.lightGrayColor().CGColor
            tempButton.addTarget(self, action: "changeCondition:", forControlEvents: .TouchUpInside)
            tempButton.tag = i
            tempButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
            
            self.conditionDropDown.addSubview(tempButton)
        }
    }
    @IBAction func chooseCondition(sender:AnyObject)
    {
        if(!conditionToggle)
        {
            UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                var tempFrame = self.conditionButton.frame
                let transRect = CGRect(x: tempFrame.origin.x, y: tempFrame.origin.y+40, width: tempFrame.width, height: 35*4)
                self.conditionDropDown.frame = transRect
                }, completion: {finished in
            })
            conditionToggle = true
        }
        else
        {
            UIView.animateWithDuration(0.15, delay: 0.1, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                var tempFrame = self.conditionButton.frame
                let transRect = CGRect(x: tempFrame.origin.x, y: tempFrame.origin.y+40, width: tempFrame.width, height: 0)
                self.conditionDropDown.frame = transRect
                }, completion: nil)
            conditionToggle = false
        }
    }
    func changeCondition(sender: UIButton!)
    {
        conditionButton.setTitle(sender.titleLabel?.text, forState: .Normal)
        UIView.animateWithDuration(0.15, delay: 0.1, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            var tempFrame = self.conditionButton.frame
            let transRect = CGRect(x: tempFrame.origin.x, y: tempFrame.origin.y+40, width: tempFrame.width, height: 0)
            self.conditionDropDown.frame = transRect
            }, completion: nil)
    }

//    @IBAction func chooseType(sender: AnyObject) {
//        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
//            var transRect = CGRect(x: 16, y: self.typeButton.frame.origin.y, width: self.view.frame.width*0.6, height:5*40)
//            self.typeDropDown.frame = transRect
//            }, completion: {finished in
//        })
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    func changeType(sender:UIButton!)
//    {
//        typeButton.setTitle(sender.titleLabel?.text, forState: .Normal)
//        UIView.animateWithDuration(0.15, delay: 0.1, options: UIViewAnimationOptions.CurveEaseIn, animations: {
//            var transRect = CGRect(x: 16, y: self.typeButton.frame.origin.y, width: self.view.frame.width*0.6, height: 0)
//            self.typeDropDown.frame = transRect
//            }, completion: nil)
//        
//    }
    
    
    func resizeImage(width:CGFloat,height:CGFloat) -> UIImage
    {
        
        let imgWidth = imageView.imageForState(.Normal)!.size.width
        let imgHeight = imageView.imageForState(.Normal)!.size.height
        
        let wratio = width/imgWidth
        let hratio = height/imgHeight
        var size = CGSize()
        if (wratio>hratio)
        {
            size = CGSize(width: imgWidth*hratio, height: imgHeight*hratio)
        }
        else
        {
            size = CGSize(width: imgWidth*wratio, height: imgHeight*wratio)
        }
        
        UIGraphicsBeginImageContext(size)
        imageView.imageForState(.Normal)!.drawInRect(CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
        
    }
    func randomizedListing()
    {
        var types = ["book","furniture","clothes","tech","misc"]
        var randind = Int(arc4random_uniform(UInt32(types.count)))
//        typeField.text = types[randind]
        randind = Int(arc4random_uniform(UInt32(types.count)))
        let randind2 = Int(arc4random_uniform(UInt32(types.count)))
        let randind3 = Int(arc4random_uniform(UInt32(types.count)))
        let name = types[randind]+types[randind2]+types[randind3]
        nameField.text = name
        let randprice = Int(arc4random_uniform(1000))
        priceField.text = String(randprice)
    }
    
    
    
    func createRequest() -> NSMutableURLRequest
    {
        
        
        let width = self.view.frame.width
        let height = self.view.frame.height
//        var size = CGSize(width: width, height: height)
        let newImage = resizeImage(width,height: height)
        
        let imageData = UIImagePNGRepresentation(newImage)
        
        let params:Dictionary = ["name":nameField.text!,"description":descriptionField.text!,"price":priceField.text!,"type":category,"school":"cmu"]
        let data:NSData!
        do{
            data = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
        }
        catch{
            print("wtf??")
            data = nil
        }
        
        let postData = NSData(data: data)

        let boundary = NSString(format: "---------------------------14737809831466499882746641449")
        let contentType = NSString(format: "multipart/form-data; boundary=%@",boundary)
        if imageData != nil
        {
            let request = NSMutableURLRequest(URL: NSURL(string:"http://\(InfoManager.sharedInstance.url)/api/listing/add")!)
            request.HTTPMethod = "POST"
            request.setValue(contentType as String, forHTTPHeaderField: "Content-Type")
            
            let body = NSMutableData()
            
            
            body.appendData(NSString(format: "\r\n--%@\r\n",boundary).dataUsingEncoding(NSUTF8StringEncoding)!)
            body.appendData(NSString(format:"Content-Disposition: form-data; name=\"data\"\r\n\r\n").dataUsingEncoding(NSUTF8StringEncoding)!)
//            body.appendData(NSString(format: "Content-Type: application/json\r\n\r\n").dataUsingEncoding(NSUTF8StringEncoding)!)
            body.appendData(postData)
            body.appendData(NSString(format: "\r\n").dataUsingEncoding(NSUTF8StringEncoding)!)
//            body.appendData(NSString(format: "%@\r\n",postData ).dataUsingEncoding(NSUTF8StringEncoding)!)
            
            body.appendData(NSString(format: "\r\n--%@\r\n", boundary).dataUsingEncoding(NSUTF8StringEncoding)!)
            body.appendData(NSString(format:"Content-Disposition: form-data; name=\"file\"; filename=\"img.jpg\"\\r\n").dataUsingEncoding(NSUTF8StringEncoding)!)
            body.appendData(NSString(format: "Content-Type: application/octet-stream\r\n\r\n").dataUsingEncoding(NSUTF8StringEncoding)!)
            body.appendData(imageData!)
            body.appendData(NSString(format: "\r\n--%@--\r\n", boundary).dataUsingEncoding(NSUTF8StringEncoding)!)
            
//            for each in params
//            {
//                println(each.key)
//                println(each.value)
//                body.appendData(NSString(format: "\r\n--%@\r\n",boundary).dataUsingEncoding(NSUTF8StringEncoding)!)
//                body.appendData(NSString(format:"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",each.key as! String).dataUsingEncoding(NSUTF8StringEncoding)!)
//                body.appendData(NSString(format: "Content-Type: application/json\r\n\r\n").dataUsingEncoding(NSUTF8StringEncoding)!)
//                body.appendData(NSString(format: "%@\r\n",each.value as! String).dataUsingEncoding(NSUTF8StringEncoding)!)
//            }
            
//            println(body)
            request.HTTPBody = body
            let postlength = NSString(format: "%d", body.length)
            request.setValue(postlength as String, forHTTPHeaderField: "Content-Length")
            return request
            
            
        }
        return NSMutableURLRequest()
    }
    
    @IBAction func submit(sender: AnyObject) {
        
        let request = createRequest()
            
        let passwordString = "\(InfoManager.sharedInstance.username):\(InfoManager.sharedInstance.token)"
        let passwordData = passwordString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        let base64Credential = passwordData!.base64EncodedStringWithOptions([])
        request.setValue("Basic \(base64Credential)", forHTTPHeaderField: "Authorization")
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()

        let authString = "Basic \(base64Credential)"
        config.HTTPAdditionalHeaders = ["Authorization": authString]
        let session = NSURLSession(configuration: config)

        let uploadTask = session.uploadTaskWithRequest(request, fromData: request.HTTPBody, completionHandler: {(let data,let response, let error) in
            if error != nil
            {
                print(error)
                return
            }
//            print(request)
//            print(sender.tag)
            self.navigationController?.viewControllers.first?.dismissViewControllerAnimated(true, completion: nil)

        })
        uploadTask.resume()
        
  

    }
    func getBookData(isbn:String)
    {
        let urlPath = "https://www.googleapis.com/books/v1/volumes?q=isbn:\(isbn)&key=\(googleKey)"
        let url:NSURL = NSURL(string: urlPath)!
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        let session = NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {
            (let data, let request, let error) in
            if error != nil
            {
                print (error)
                return
            }
            let json: NSDictionary!
            do{
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSDictionary
            }
            catch
            {
                json = nil
            }
            
            for each in json
            {
                let (key,value) = each
                if (key as! String) == "items"
                {
                    let (dict) = value
                    for k in dict as! NSArray
                    {
                        let info = k as! NSDictionary
                        for j in info
                        {
                            if j.key as! String == "volumeInfo"
                            {
                                let bookInfo = j.value as! NSDictionary
//                                print(bookInfo["imageLinks"])
                                let imageDict = bookInfo["imageLinks"] as! NSDictionary
                                self.getThumbnailForBook(imageDict["thumbnail"] as! String)
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.nameField.text = bookInfo["title"] as? String
                                    self.descriptionField.text = bookInfo["description"] as! String
                                    self.descriptionField.textColor = UIColor.blackColor()
                                })
                                
                                
                                
                            }
//                            println(j.value)
                        }
                    }
                }
                
            }
            //            println(json)
            
        })
        session.resume()
    }
    func getThumbnailForBook(urlPath:String)
    {
        let url:NSURL = NSURL(string: urlPath)!
        NSURLSession.sharedSession().dataTaskWithURL(url, completionHandler: {
            (let data, let request, let error) in
            if error != nil
            {
                print(error)
                return
            }
            let image = UIImage(data: data!)
            dispatch_async(dispatch_get_main_queue(), {
                self.imageView2.setImage(image, forState: .Normal)
            })
        }).resume()
    }
    @IBAction func cancel(sender: AnyObject) {
        InfoManager.sharedInstance.shouldSetup = false
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBAction func presentCameraSheet(sender:AnyObject)
    {
        self.currImage = sender.tag
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default, handler: {(let x) in self.camera(x)})
        actionSheet.addAction(cameraAction)
        let libraryAction  = UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.Default, handler: {(let some) in self.cameraRoll(some)})
        actionSheet.addAction(libraryAction)
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler:
            {(let some) in
                actionSheet.dismissViewControllerAnimated(true, completion: nil)
        })
        actionSheet.addAction(cancelButton)
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    @IBAction func submit100(sender: AnyObject) {
        for (var i = 0;i<75;i++)
        {
            let x = UIButton()
            x.tag = i
            randomizedListing()
            submit(x)
            print(i)
        }
    }
    
//IMAGES stuff
    @IBAction func camera(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.Camera) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.Camera
                imagePicker.mediaTypes = [kUTTypeImage as String]
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true, 
                    completion: nil)
                newMedia = true
        }
    }
    @IBAction func cameraRoll(sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.SavedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.PhotoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as String]
                imagePicker.allowsEditing = false
                self.presentViewController(imagePicker, animated: true,
                    completion: nil)
                newMedia = false
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if mediaType == (kUTTypeImage as String) {
            let image = info[UIImagePickerControllerOriginalImage]
                as! UIImage
            
            imageViewList[currImage].setImage(image, forState: .Normal)
            
            if (newMedia == true) {
                UIImageWriteToSavedPhotosAlbum(image, self,
                    "image:didFinishSavingWithError:contextInfo:", nil)
            } else if mediaType == (kUTTypeMovie as String) {
                // Code to support video here
            }
            
        }
    }
    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
//        
//        let mediaType = info[UIImagePickerControllerMediaType] as! String
//        
//        self.dismissViewControllerAnimated(true, completion: nil)
//        
//        if mediaType == (kUTTypeImage as String) {
//            let image = info[UIImagePickerControllerOriginalImage]
//                as! UIImage
//            
//            imageViewList[currImage].setImage(image, forState: .Normal)
//            
//            if (newMedia == true) {
//                UIImageWriteToSavedPhotosAlbum(image, self,
//                    "image:didFinishSavingWithError:contextInfo:", nil)
//            } else if mediaType == (kUTTypeMovie as String) {
//                // Code to support video here
//            }
//            
//        }
//    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafePointer<Void>) {
        
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                message: "Failed to save image",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                style: .Cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true,
                completion: nil)
        }
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        
        UIView.animateWithDuration(0.15, delay: 0.1, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            var tempFrame = self.conditionButton.frame
            let transRect = CGRect(x: tempFrame.origin.x, y: tempFrame.origin.y+40, width: tempFrame.width, height: 0)
            self.conditionDropDown.frame = transRect
            }, completion: nil)
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
//        print(textView.text)
        if(textView.text == "Write a description...")
        {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        textView.becomeFirstResponder()
    }
    func textViewDidEndEditing(textView: UITextView) {
        if(textView.text == "")
        {
            textView.text = "Write a description..."
            textView.textColor = UIColor.lightGrayColor()
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
