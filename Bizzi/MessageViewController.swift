//
//  MessageViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/30/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class MessageViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var frameView: UIView!

    @IBOutlet weak var scrollView: UIScrollView!

    var product = InfoManager.sharedInstance.currentProdcut
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after  the view.
        sellerNameLabel.text = product.seller
        priceLabel.text = "$\(product.price)"
        imgView.image = product.image
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)

        
    }
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let info:NSDictionary = notification.userInfo!
        var keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        
        let keyboardHeight:CGFloat = 216
        
        var height:CGFloat = self.frameView.frame.origin.y - keyboardHeight
        
        var animationDuration:NSTimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        var adsf:CGFloat = 0
        
//        UIView.animateWithDuration(animationDuration, delay: adsf, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
//            var temp = CGRect(x: 0, y: height, width: self.view.bounds.width, height: self.view.bounds.height)
//            self.frameView.frame = CGRect()
//            
//            }, completion: nil)
        print(height)
        UIView.animateWithDuration(animationDuration, animations: {()->Void in
            var temp = CGRect(x: 0, y: -208, width: self.frameView.bounds.width, height: self.frameView.bounds.height)
            self.frameView.frame = temp
            
        })
        
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var info:NSDictionary = notification.userInfo!
        var keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        
        var keyboardHeight:CGFloat = 216
        var height = self.frameView.frame.origin.y + keyboardHeight
        var animationDuration:NSTimeInterval = info[UIKeyboardAnimationDurationUserInfoKey] as! NSTimeInterval
        
        print(keyboardSize.height)
        UIView.animateWithDuration(animationDuration, delay: 0.25, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.frameView.frame = CGRectMake(0, 50, self.frameView.bounds.width, self.frameView.bounds.height)
            }, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
