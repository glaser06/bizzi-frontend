//
//  TransactionListViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 8/2/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class TransactionListViewController: UITableViewController {

    @IBOutlet weak var segView: UISegmentedControl!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var products = Array(count: 2, repeatedValue: [Product]())
    
    var thumbnailList:[UIImageView] = []
    
    var segIndex:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            menuButton.target = self//.revealViewController()
            menuButton.action = "changeView:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().toggleAnimationType = SWRevealToggleAnimationType.EaseOut
            //            self.revealViewController().toggleAnimationDuration = 0.25
            self.revealViewController().replaceViewAnimationDuration = 0
            
            self.revealViewController().tapGestureRecognizer()
            self.revealViewController().panGestureRecognizer()
        }
        // Do any additional setup after  the view.
        getProducts()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func changeView(sender:AnyObject)
    {
        InfoManager.sharedInstance.shouldSetup = true
        self.revealViewController().revealToggle(sender)
    }
    
    func getProducts()
    {
        
        for (var i=0;i<20;i++)
        {
            let product = Product(name: "something \(i)", type: "book", price: "867", seller: "dev1", img: ["asdf"])
            product.details = "asdf"
            product.listingID = "asdf"
            product.image = UIImage(named: "tech")
            self.products[0].append(product)
        }

        for (var i=0;i<20;i++)
        {
            let product = Product(name: "other thing \(i)", type: "fashion", price: "257", seller: "dev1", img: ["asdf"])
            product.details = "asdf"
            product.listingID = "asdf"
            product.image = UIImage(named: "tech")
            self.products[1].append(product)
        }
        
//        self.listProducts()

    }
    func listProducts()
    {
        
    }
    func getImage()
    {
        
    }
    func getThumbnail(index:Int)
    {
        let params:NSDictionary = ["width":187,"height":281]
        RequestLibrary.postRequest("http://\(InfoManager.sharedInstance.url)/api/listing/image/\(products[segIndex][index].imgID[0])/thumbnail", params: params, handler: {(let data, let response, let error) in
            if error != nil
            {
                print(error)
                return
            }
            dispatch_async(dispatch_get_main_queue(), {
                
                self.thumbnailList[index].removeFromSuperview()
                let newImage = self.resizeImage(self.view.frame.width, height: self.view.frame.width - 60, image: UIImage(data: data!)!)
                
                let center = self.view.frame.width/2
                let imgcenter = newImage.size.width/2
                let x = center-imgcenter
                
                self.thumbnailList[index].frame = CGRect(origin: CGPoint(x: x, y: 0), size: newImage.size)
                
                self.thumbnailList[index].image = newImage
                
//                self.productViewList[index].addSubview(self.thumbnailList[index])
                
            })
        }).resume()
    }
    
    func resizeImage(width:CGFloat,height:CGFloat,image:UIImage) -> UIImage
    {
        
        let imgWidth = image.size.width
        let imgHeight = image.size.height
        
        let wratio = width/imgWidth
        let hratio = height/imgHeight
        var size = CGSize()
        if (wratio>hratio)
        {
            size = CGSize(width: imgWidth*hratio, height: imgHeight*hratio)
        }
        else
        {
            size = CGSize(width: imgWidth*wratio, height: imgHeight*wratio)
        }
        
        UIGraphicsBeginImageContext(size)
        image.drawInRect(CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
        
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as UITableViewCell?
        if cell == nil
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let imgView = cell?.viewWithTag(5) as! UIImageView
        imgView.image = UIImage(named: "tech")


        

        
        let nameLabel = cell?.viewWithTag(1) as! UILabel
        nameLabel.text = products[segIndex][indexPath.row].name


        let sellerLabel = cell?.viewWithTag(3) as! UILabel
        sellerLabel.text = products[segIndex][indexPath.row].seller

        
        let priceLabel = cell?.viewWithTag(2) as! UILabel
        priceLabel.text = "$\(products[segIndex][indexPath.row].price)"
        
        let categoryLabel = cell?.viewWithTag(4) as! UILabel
        categoryLabel.text = products[segIndex][indexPath.row].type

        
        
        
        
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if segIndex == 0
        {
            InfoManager.sharedInstance.currentProdcut = products[segIndex][indexPath.row]
            let messageVC = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
            self.navigationController?.pushViewController(messageVC, animated: true)
            
        }
        
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products[segIndex].count
    }
    
    
    
    @IBAction func segmentChange(sender: AnyObject) {
        if segView.selectedSegmentIndex == 0
        {
            segIndex = 0
            self.tableView.reloadData()
        }
        else
        {
            segIndex = 1
            self.tableView.reloadData()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
