//
//  ProductDetailViewController.swift
//  
//
//  Created by Zaizen Kaegyoshi on 7/11/15.
//
//

import UIKit

class ProductDetailViewController: UIViewController {

    
    @IBOutlet weak var imageView: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var detailtextView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
//    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var sellerLabel: UILabel!

    @IBOutlet weak var counterButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    
    var product = InfoManager.sharedInstance.currentProdcut
    
    var showBuy = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        


        
//        var buyButton = UIBarButtonItem(title: "Buy", style: UIBarButtonItemStyle.Plain, target: self, action: "buy")
//        self.navigationItem.rightBarButtonItem = buyButton
//        InfoManager.sharedInstance.navBarColor = buyButton.backgroundColor
        // Do any additional setup after  the view.
        let width = imageView.frame.width
        let height = imageView.frame.height
        let newImage = resizeImage(width, height: height)

        let center = self.view.frame.width/2
        let imgcenter = newImage.size.width/2
        let x = center-imgcenter
        
        imageView.frame = CGRect(origin: CGPoint(x: x, y: 0), size: newImage.size)
        if let _ = product.image
        {
            imageView.setBackgroundImage(product.image, forState: UIControlState.Normal)
        }
        else
        {
            self.getImage()

        }
//        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.scrollView.frame.height)
        
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Discover", style: UIBarButtonItemStyle.Bordered, target: nil, action: nil)
//        self.navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        
        nameLabel.text = "   "+(product.name)
        nameLabel.layer.borderColor = UIColor.lightGrayColor().CGColor
        nameLabel.layer.borderWidth = 0.7
        priceLabel.text = "    $" + String(product.price)
        priceLabel.layer.borderColor = UIColor.lightGrayColor().CGColor
        priceLabel.layer.borderWidth = 0.7
        
        detailtextView.text = product.details
        
//        typeLabel.text = product.type
        detailtextView.sizeToFit()
        detailtextView.layoutIfNeeded()
//        detailtextView.layer.borderWidth = 0.7
//        detailtextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        detailtextView.scrollEnabled = false
        detailtextView.frame = CGRect(x: detailtextView.frame.origin.x, y: detailtextView.frame.origin.y, width: self.view.frame.width, height: detailtextView.frame.height)
        detailtextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        detailtextView.layer.borderWidth = 0.7
        
        sellerLabel.text = "   from " + product.seller
//        sellerLabel.frame = 
        
        buyButton.backgroundColor = MainListViewController.bizziColor
        let tempFr = detailtextView.frame
        sellerLabel.frame = CGRect(x: 0, y: tempFr.origin.y+tempFr.height, width: sellerLabel.frame.width, height: sellerLabel.frame.height)
//        buyButton.layer.cornerRadius = 5
//        messageButton.layer.cornerRadius = 5
//        counterButton.layer.cornerRadius = 5
        
        
        if let status = product.status
        {

            if status == "bought"
            {
                buyButton.removeFromSuperview()
                messageButton.removeFromSuperview()
//                counterButton.removeFromSuperview()
            }
            else if(product.seller == InfoManager.sharedInstance.username)
            {
                buyButton.removeFromSuperview()
                messageButton.removeFromSuperview()
//                counterButton.removeFromSuperview()
            }
            
        }
//        self.scrollView.sizeToFit()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func imageViewChange(sender: AnyObject) {
        let ImageVC = self.storyboard?.instantiateViewControllerWithIdentifier("ImageViewController") as! ImageViewController
//        self.navigationController!.pushViewController(ImageVC, animated: true)
        self.presentViewController(ImageVC, animated: true, completion: nil)

    }
    @IBAction func buy()
    {
        let BuyVC = self.storyboard?.instantiateViewControllerWithIdentifier("BuyNavController") as! UINavigationController
//        self.navigationController?.pushViewController(BuyVC, animated: true)
        self.presentViewController(BuyVC, animated: true, completion: nil)
//        self.navigationItem.hidesBackButton = true
    }
    func resizeImage(width:CGFloat,height:CGFloat) -> UIImage
    {
        if product.image == nil
        {
            return UIImage()
        }
        let imgWidth = product.image.size.width
        let imgHeight = product.image.size.height
        
        let wratio = width/imgWidth
        let hratio = height/imgHeight
        var size = CGSize()
        if (wratio>hratio)
        {
            size = CGSize(width: imgWidth*hratio, height: imgHeight*hratio)
        }
        else
        {
            size = CGSize(width: imgWidth*wratio, height: imgHeight*wratio)
        }
        
        UIGraphicsBeginImageContext(size)
        product.image.drawInRect(CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
        
    }
    func getImage()
    {
        
//        if (index > self.products.count)
//        {
//            return
//        }
        RequestLibrary.getRequest("http://\(InfoManager.sharedInstance.url)/api/listing/image/\(product.imgID[0])",handler:{(let data, let response, let error) in
            if error != nil
            {
                print(error)
                return
            }
            
            
            let image = UIImage(data: data!)
            self.product.image = image
            self.imageView.setBackgroundImage(image, forState: UIControlState.Normal)
            
            
            
        }).resume()
        
        
    }
    @IBAction func message(sender:AnyObject)
    {
        let messageVC = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
        self.navigationController?.pushViewController(messageVC, animated: true)
    }
    

    @IBAction func dismissView(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
