//
//  SecondViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/10/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var listings:[Product] = []
    var productViewList:[UIButton] = []
    var thumbnailList:[UIImageView] = []
    
    var page = 0
    var maxContent = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after  the view, typically from a nib.
        if self.revealViewController() != nil {
            menuButton.target = self//.revealViewController()
            menuButton.action = "changeView:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().toggleAnimationType = SWRevealToggleAnimationType.EaseOut
            
            self.revealViewController().tapGestureRecognizer()
            self.revealViewController().panGestureRecognizer()
        }
        nameLabel.text = InfoManager.sharedInstance.username
        
        scrollView.layer.borderColor = UIColor.blackColor().CGColor
        scrollView.layer.borderWidth = 1.0
        
        getUserListing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getUserListing()
    {
        let params = ["page":0]
        let uploadTask = RequestLibrary.postRequest("http://\(InfoManager.sharedInstance.url)/api/listing/list/user", params: params, handler: {(let data, let response, let error) in
            
            if error != nil
            {
                print(error)
                return
            }
            print(response)
            let json: AnyObject?
            do{
                json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves)
            }catch{
                json = nil
            }
            //            println(json)
            if let json1 = json as? NSDictionary
            {
                for each in json1
                {
                    //                    println(each.key)
                    if (each.key as! String == "bought")
                    {
//                        print(each.value)
                        for list in each.value as! NSDictionary
                        {
                            let (key,value) = list
                            let listing = value as! NSDictionary
                            let price = listing["price"] as! NSArray
                            let imgid = listing["image_ids"] as! NSArray
                            //                            var listing = list as! NSDictionary
                            let product = Product(name: listing["name"] as! String, type: "misc", price: price[0] as! String, seller: listing["user_name"] as! String, img: imgid as! [String])
                            product.details = listing["description"] as! String
                            product.listingID = (key as! String)
                            product.status = "bought"
                            self.listings.append(product)
                            
                        }
                    }
                }
                //                for each in json1
                //                {
                //                    var listing = each.value as! NSDictionary
                //                    var product = Product(name: listing["name"] as! String, type: "misc", price: String(listing["price"] as! Int), seller: listing["user_name"] as! String, img: listing["image_ids"] as! [Int])
                //                    product.details = listing["description"] as! String
                //                    self.listings.append(product)
                //                }
            }
            dispatch_async(dispatch_get_main_queue(), {
                
                self.displayListing()
            })
            
        })
        uploadTask.resume()
    }
    func displayListing()
    {
        let width = self.view.frame.width
        let height = width
        
        var loadCount = self.listings.count
        if loadCount>self.maxContent
        {
            loadCount = self.maxContent
        }
        for(var i=self.maxContent-20;i<loadCount;i++)
        {
            
            var tempFrame = CGRect(x:0, y: height*CGFloat(i), width: width, height: height)
            
            let productView = UIButton(frame:tempFrame)
            productView.backgroundColor = UIColor.whiteColor()
            productView.layer.borderWidth = 0.5
            productView.layer.borderColor = UIColor.blackColor().CGColor
//            productView.addTarget(self, action: "detailViewChange:", forControlEvents: UIControlEvents.TouchUpInside)
            
            getImage(i)
            getThumbnail(i)
            let thumbnail = UIImage()
            
            let imageView = UIImageView(image: thumbnail)
            thumbnailList.append(imageView)
            imageView.frame = CGRect(x: 0, y: 0, width: width, height: height-60)
            productView.addSubview(imageView)
            tempFrame = CGRect(x: 5, y: height-60, width: width-10, height: 30)
            let nameLabel = UILabel(frame: tempFrame)
            nameLabel.text = self.listings[i].name
            productView.addSubview(nameLabel)
            tempFrame = CGRect(x:5,y: height-30,width:width-10,height:30)
            let priceLabel = UILabel(frame: tempFrame)
            priceLabel.text = String(self.listings[i].price)
            productView.addSubview(priceLabel)
            
            
            productViewList.append(productView)
            scrollView.addSubview(productView)
            productView.tag = i
            
        }
        scrollView.contentSize = CGSize(width: width, height: height*CGFloat(loadCount))
        
        
    }
    func getThumbnail(index:Int)
    {
        let params:NSDictionary = ["width":187,"height":281]
        RequestLibrary.postRequest("http://\(InfoManager.sharedInstance.url)/api/listing/image/\(listings[index].imgID[0])/thumbnail", params: params, handler: {(let data, let response, let error) in
            if error != nil
            {
                print(error)
                return
            }
            dispatch_async(dispatch_get_main_queue(), {
                
                self.thumbnailList[index].removeFromSuperview()
                let newImage = self.resizeImage(self.view.frame.width, height: self.view.frame.width - 60, image: UIImage(data: data!)!)
                
                let center = self.view.frame.width/2
                let imgcenter = newImage.size.width/2
                let x = center-imgcenter
                
                self.thumbnailList[index].frame = CGRect(origin: CGPoint(x: x, y: 0), size: newImage.size)
                
                self.thumbnailList[index].image = newImage
                
                self.productViewList[index].addSubview(self.thumbnailList[index])
                
            })
        }).resume()
        
    }
    func getImage(index:Int)
    {
        
        
        RequestLibrary.getRequest("http://\(InfoManager.sharedInstance.url)/api/listing/image/\(listings[index].imgID[0])",handler:{(let data, let response, let error) in
            if error != nil
            {
                print(error)
                return
            }
            
            
            let image = UIImage(data: data!)
            self.listings[index].image = image
            
            
        }).resume()
        
        
    }
    func resizeImage(width:CGFloat,height:CGFloat,image:UIImage) -> UIImage
    {
        
        let imgWidth = image.size.width
        let imgHeight = image.size.height
        
        let wratio = width/imgWidth
        let hratio = height/imgHeight
        var size = CGSize()
        if (wratio>hratio)
        {
            size = CGSize(width: imgWidth*hratio, height: imgHeight*hratio)
        }
        else
        {
            size = CGSize(width: imgWidth*wratio, height: imgHeight*wratio)
        }
        
        UIGraphicsBeginImageContext(size)
        image.drawInRect(CGRect(origin: CGPoint(x: 0, y: 0), size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
        
    }
    
    
    func detailViewChange(sender:AnyObject)
    {
        InfoManager.sharedInstance.currentProdcut = self.listings[sender.tag]
        let ProductDVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProductDetailViewController") as! ProductDetailViewController
        self.navigationController!.pushViewController(ProductDVC, animated: true)
        //        self.presentViewController(ProductDVC, animated: true, completion: nil)
    }
    
    func changeView(sender:AnyObject)
    {
        InfoManager.sharedInstance.shouldSetup = true
        self.revealViewController().revealToggle(sender)
    }
    
    @IBAction func logout(sender: AnyObject) {
        RequestLibrary.getRequest("http://\(InfoManager.sharedInstance.url)/api/login/logout", handler: {(let data, let response, let error) in
            if error != nil
            {
                print(error)
                return
            }
            InfoManager.sharedInstance.username = ""
            InfoManager.sharedInstance.token = ""
            dispatch_async(dispatch_get_main_queue(), {
                
                let mainVC = self.storyboard?.instantiateViewControllerWithIdentifier("MainNavController") as! UINavigationController
                self.revealViewController().setFrontViewController(mainVC, animated: true)
            })
            
//            print(response)
        }).resume()
    }
}