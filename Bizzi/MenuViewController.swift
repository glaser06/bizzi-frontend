//
//  MenuViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/11/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class MenuViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewDidDisappear(animated: Bool) {
        self.revealViewController().frontViewController.view.userInteractionEnabled = true
    }
    override func viewWillAppear(animated: Bool) {
        self.revealViewController().frontViewController.view.userInteractionEnabled = false
        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        println(indexPath.row)
        if indexPath.row == 2
        {
            if InfoManager.sharedInstance.username == "" || InfoManager.sharedInstance.token == ""
            {
                let loginVC = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
                self.presentViewController(loginVC, animated: true, completion: nil)
                InfoManager.sharedInstance.shouldSetup  = true
            }
            else{
                let SellVC = self.storyboard?.instantiateViewControllerWithIdentifier("CategoryNavController") as! UINavigationController
                self.presentViewController(SellVC, animated: true, completion: nil)
            }
//            self.revealViewController().revealToggle(false)
            self.revealViewController().revealToggleAnimated(true)
            self.revealViewController().frontViewController.view.userInteractionEnabled = true
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = super.tableView(tableView, cellForRowAtIndexPath: indexPath)
//        cell.layer.borderColor = UIColor.whiteColor().CGColor
//        cell.layer.borderWidth = 0.3
        if (indexPath.row == 0)
        {
            let nameLabel = UILabel(frame: CGRect(x: 75, y: 26, width: 150, height: 30))
            nameLabel.text = InfoManager.sharedInstance.username
            nameLabel.textColor = UIColor.whiteColor()
            nameLabel.font = UIFont(name: "Avenir Book", size: 22)
            cell.addSubview(nameLabel)
            
            return cell
        }
        else if(indexPath.row < 6)
        {
            let topView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 1))
            topView.backgroundColor = UIColor.darkGrayColor()
            cell.addSubview(topView)
            return cell
        }
        else {

            return cell
        }
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
