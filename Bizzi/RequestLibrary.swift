//
//  RequestLibrary.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/13/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import Foundation

public class RequestLibrary {
    
    
    static func postRequest(url:String, params:NSDictionary!,handler:((NSData?, NSURLResponse?, NSError?) -> Void)) -> NSURLSessionDataTask
    {
        let urlPath = url
        let url: NSURL = NSURL(string: urlPath)!
        var request1 = NSURLRequest(URL: url)
        let request: NSMutableURLRequest = request1.mutableCopy() as! NSMutableURLRequest
        request.HTTPMethod = "POST"
        let passwordString = "\(InfoManager.sharedInstance.username):\(InfoManager.sharedInstance.token)"
        let passwordData = passwordString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        let base64Credential = passwordData!.base64EncodedStringWithOptions([])
        request.setValue("Basic \(base64Credential)", forHTTPHeaderField: "Authorization")
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let authString = "Basic \(base64Credential)"
        config.HTTPAdditionalHeaders = ["Authorization": authString]
        let session = NSURLSession(configuration: config)
        if params != nil
        {
            let postData:NSData!
            do {
                postData = try NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: []))
            }
            catch{
                postData = nil
            }
            
            request.HTTPBody = postData
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
        }
        request1 = request.copy() as! NSURLRequest
        let uploadTask = session.dataTaskWithRequest(request1, completionHandler: handler)

        return uploadTask
        
        
    }
    static func getRequest(url:String,handler:((NSData?, NSURLResponse?, NSError?) -> Void)) -> NSURLSessionDataTask
    {
        let urlPath = url
        let url: NSURL = NSURL(string: urlPath)!
        var request1 = NSURLRequest(URL: url)
        let request = request1.mutableCopy() as! NSMutableURLRequest
        request.HTTPMethod = "GET"
        let passwordString = "\(InfoManager.sharedInstance.username):\(InfoManager.sharedInstance.token)"
        let passwordData = passwordString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        let base64Credential = passwordData!.base64EncodedStringWithOptions([])
        request.setValue("Basic \(base64Credential)", forHTTPHeaderField: "Authorization")
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let authString = "Basic \(base64Credential)"
        config.HTTPAdditionalHeaders = ["Authorization": authString]
        let session = NSURLSession(configuration: config)
        
        request1 = request.copy() as! NSURLRequest
        let uploadTask = session.dataTaskWithRequest(request1, completionHandler: handler)
        
        return uploadTask
    }
    static func colorFromRGB(red:CGFloat,green:CGFloat,blue:CGFloat)->UIColor
    {
        let r = red/255
        let g = green/255
        let b = blue/255
        
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }
    
    
}