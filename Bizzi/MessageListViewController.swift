//
//  MessageListViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 8/4/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class MessageListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            menuButton.target = self//.revealViewController()
            menuButton.action = "changeView:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().toggleAnimationType = SWRevealToggleAnimationType.EaseOut
            //            self.revealViewController().toggleAnimationDuration = 0.25
            self.revealViewController().replaceViewAnimationDuration = 0
            
            self.revealViewController().tapGestureRecognizer()
            self.revealViewController().panGestureRecognizer()
        }
        // Do any additional setup after  the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func changeView(sender:AnyObject)
    {
        InfoManager.sharedInstance.shouldSetup = true
        self.revealViewController().revealToggle(sender)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell1") as UITableViewCell!
        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
