//
//  ImageViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/12/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after  the view.
        let size = InfoManager.sharedInstance.currentProdcut.image.size
        
        let ycenter = size.height/2
        
        
        imageView.frame = CGRect(origin: CGPoint(x: 0, y: self.view.frame.height/2-ycenter), size: size)
        imageView.image = InfoManager.sharedInstance.currentProdcut.image
        self.view.backgroundColor = UIColor.blackColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
