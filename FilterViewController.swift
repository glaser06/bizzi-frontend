//
//  FilterViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/22/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {
    
    var filterView: UIScrollView!
    
    @IBOutlet weak var coverView: UIView!
    var yPos:CGFloat = 106
    
    var filterRect:CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.filterView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width*0.6, height: 45+5*40))
//        print(filterView.frame.width)
        let ymid = filterView.frame.height/2 + filterView.frame.origin.y
        let ypos = filterView.frame.origin.y + ((self.view.frame.height/2) - ymid) - 60
        let xmid = filterView.frame.width/2 + filterView.frame.origin.x
        let xpos = filterView.frame.origin.x + ((self.view.frame.width/2) - xmid)
        self.filterView.frame = CGRect(x: xpos, y: ypos, width: self.view.frame.width*0.6, height: 45+5*40)
        self.filterView.alpha = 0
        
        filterRect = self.filterView.frame
//        print()
//        print(self.view.frame.width - (filterView.frame.origin.x + filterView.frame.width))
        
        makeFilterView()
        self.view.addSubview(filterView)
//        self.navigationController?.navigationBarHidden = true

        // Do any additional setup after  the view.
    }
    override func viewDidAppear(animated: Bool) {
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {
            self.coverView.backgroundColor = UIColor(white: 0.3, alpha: 0.3)
            let transRect = self.filterRect
            self.filterView.frame = transRect
            self.filterView.alpha = 1
            }, completion: {finished in
        })

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func makeFilterView()
    {
        self.filterView.layer.cornerRadius = 10
        
        let filterTitle = UILabel(frame: CGRect(x: 0, y: 0, width: self.filterView.frame.width, height: 50))
        filterTitle.text = "Filter"
        filterTitle.font = filterTitle.font.fontWithSize(20)
        filterTitle.textAlignment = NSTextAlignment.Center
        filterTitle.backgroundColor = UIColor.lightGrayColor()
        self.filterView.backgroundColor = UIColor.whiteColor()
        self.filterView.addSubview(filterTitle)
        var types = ["Book","Furniture","Tech","Fashion","Misc"]
        
        for(var i=0;i<5;i++)
        {
            let tempButton = UIButton(frame: CGRect(x: 0, y: CGFloat(45+40*i), width: filterView.frame.width, height: CGFloat(40)))
            tempButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
            tempButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            tempButton.setTitle(types[i], forState: UIControlState.Normal)
            tempButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
            tempButton.layer.borderWidth = 0.4
            tempButton.layer.borderColor = UIColor.lightGrayColor().CGColor
            tempButton.addTarget(self, action: "changeType:", forControlEvents: .TouchUpInside)
            tempButton.tag = i
            self.filterView.addSubview(tempButton)

        }

        
    }
    func dismissFilter(handler:(Bool -> Void))
    {
        UIView.animateWithDuration(0.15, delay: 0.1, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.coverView.backgroundColor = UIColor(white: 1, alpha: 0)
            let transRect = self.filterRect
            self.filterView.frame = transRect
            self.filterView.alpha = 0
            }, completion: handler)
    }
    func changeType(sender:AnyObject)
    {
        self.dismissFilter({finished in
            let parent = self.parentViewController as! UINavigationController
            let main = parent.viewControllers[0] as! MainListViewController
            main.changeType(sender)
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        })
        

        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.dismissFilter({finished in
            self.viewDidDisappear(false)
            let parent = self.parentViewController as! UINavigationController
            let main = parent.viewControllers[0] as! MainListViewController
            main.toggleFilter(nil)
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
