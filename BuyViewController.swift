//
//  BuyViewController.swift
//  Bizzi
//
//  Created by Zaizen Kaegyoshi on 7/14/15.
//  Copyright (c) 2015 Bizzi. All rights reserved.
//

import UIKit
import Venmo_iOS_SDK

class BuyViewController: UIViewController {
    
    @IBOutlet weak var priceField: UITextField!
    override func viewWillAppear(animated: Bool) {
//        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 65))
//        navBar.barTintColor = MainListViewController.bizziColor
//        
//        self.view.addSubview(navBar)
//        
//        var navCont = UINavigationController(rootViewController: self)
        
        
        let cancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancel")

        self.navigationItem.leftBarButtonItem = cancel

        priceField.becomeFirstResponder()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after  the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buy(sender: AnyObject) {
        let id = InfoManager.sharedInstance.currentProdcut.listingID
        let params = ["id":id]
        RequestLibrary.postRequest("http://\(InfoManager.sharedInstance.url)/api/listing/buy", params: params, handler: {(let data, let request,let error) in
            if error != nil
            {
                print(error)
                return
            }
//            print(request)
            InfoManager.sharedInstance.currentProdcut.status = "bought"
            dispatch_async(dispatch_get_main_queue(), {
                let messageVC = self.storyboard?.instantiateViewControllerWithIdentifier("MessageVC") as! MessageViewController
                let messageListNav = self.storyboard?.instantiateViewControllerWithIdentifier("MessageNavController") as! UINavigationController
//                self.navigationController?.pushViewController(transactionVC, animated: true)
//                self.navigationItem.hidesBackButton = true
                
                self.navigationController?.viewControllers.first?.dismissViewControllerAnimated(false, completion: nil)
                
                let temp = self.navigationController?.viewControllers.first?.presentingViewController as! SWRevealViewController
                temp.setFrontViewController(messageListNav, animated: false)
                messageListNav.pushViewController(messageVC, animated: false)
            })
            
            
            
        }).resume()
    }
    func cancel()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
